# Variant 19(variant "в" in 3.9 and "г" in 3.19)
from cmath import sqrt

# Task 3.4
print("Task 3.4")


def function_a():
    n = 3
    x = 2
    i = 0
    result = 0
    while (n - i) >= 0:
        y = x ** (n - i)
        i += 1
        result += y
    print(result)

function_a()


def function_b():
    n = 4
    x = 1
    i = 0
    result = 0
    while (n - i) >= 0:
        y = x ** (2 ** (n - i))
        i += 1
        result += y
    print(result)


function_b()


def function_c():
    n = 3
    x = 1
    i = 0
    result = 0
    while (n - i) >= 0:
        y = x ** (3 ** (n - i))
        i += 1
        result += y
    print(result)


function_c()


def function_d():
    n = 4
    x = 1
    y = 2
    z = 0
    i = 0
    result = 0
    while (n - i) >= 0:
        z = (x ** (2 ** (n - i))) * (y ** (n - i))
        i += 1
        result += z
    print(result)


function_d()


def function_e():
    n = 5
    x = -1
    y = 0
    i = 1
    result = 0
    while i <= n:
        y = x ** (i ** 2)
        i += 1
        result += y
    print(result)


function_e()

# Task 3.7
print("Task 3.7")

n = 3


def several_square_roots(n):
    if n > 0:
        next_call = several_square_roots(n - 1)
        result = sqrt(2 + next_call)

    else:
        result = 0
    return result
print(several_square_roots(n))


def several_square_roots_with_n(n):
    if n > 0:
        i = n - 1
        if i >= 0:
            next_call = 3 * (n - i) + several_square_roots(n - 1)
            result = sqrt(next_call)
            i -= 1
        else:
            result = 0
    else:
        result = 0
    return result
print(several_square_roots_with_n(n))

# Task 3.9
print("Task 3.9, variant в")


def sequence(k):
    factorial = 1
    x = 2
    for i in range(1, k + 1):
        factorial = factorial * i
    number_to_find = (x ** k) / factorial
    print(number_to_find)

sequence(4)


# Task 3.19, variant г
print("Task 3.19")

def sum_of_sequence():
    k = input("Please, enter number more than 2 or equal. k = ")
    k = int(k)
    a = [1, 2]
    b = [5, 5]
    if k >= 2 and (type(k) == int):
        for n in range(1, k + 2):
            a.insert(len(a), a[n - 2] + b[n] / 2)
            b.insert(len(b), pow(b[n - 2], 2) - a[n - 1])
            s = pow((a[n] / b[n]), n)
            s += s
        print(s)
    else:
        print("Error, k have to be natural number more than 2 or equal")
sum_of_sequence()

# Task 3.39
print("Task 3.39")

def give_number1():
    n = 10
    i = 0
    line_of_numbers = '10'
    for i in range(0, 21):
       n = n ** 2
       line_of_numbers += f'{n}'
       i += i
    line_of_numbers.split()
    k = int(input("Enter position of number you want to get: "))
    print(line_of_numbers[k-1])
give_number1()

def give_number2():
    n = 1
    i = 0
    line_of_numbers = '10'
    for i in range(0, 80):
        n = n + 1
        line_of_numbers += f'{n}'
        i += i
    line_of_numbers.split()
    k = int(input("Enter position of number you want to get: "))
    print(line_of_numbers[k])
give_number2()

def give_number3():
    n = 1
    i = 0
    line_of_numbers = '10'
    for i in range(0, 80):
        n = n + 1
        line_of_numbers += f'{(n ** 2)}'
        i += i
    line_of_numbers.split()
    k = int(input("Enter position of number you want to get: "))
    print(line_of_numbers[k-1])
give_number3()

def give_number4():
    num1 = 1
    num2 = 1
    i = 0
    line_of_numbers = '011'
    while (num1 + num2) < 700:
        num1, num2 = num2, num1 + num2
        line_of_numbers += f'{num2}'
    line_of_numbers.split()
    print(line_of_numbers)
    k = int(input("Enter position of number you want to get: "))
    print(line_of_numbers[k-1])
give_number4()