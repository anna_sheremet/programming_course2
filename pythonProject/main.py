# The first task
Y = 6.673

print('Power of attraction: ')
def power_of_attraction():
     m1 = 10
     m2 = 25
     r = 5
     result = Y * m1 * m2 / (r ** 2)
     print(f"{result} * 10^(-11) H")
power_of_attraction()

# Second task

print('Solving of expressions: ')
def first_expression():
    a = 12
    b = 3
    c = 4
    d = 6
    f = 1
    result = a * a - c + (a * a - c) / (b * c + c / (d + 1 / f + b * c))
    print(result)
first_expression()

def second_expression():
    a = 2
    b = 4
    c = 7
    result = (1 / a + 1 / b) / (1 / a - 1 / c)
    print(result)
second_expression()

def third_expression():
    a = 2
    b = 3
    c = 1
    d = 0.5
    result = (a + b) / (c + d) - a * b / (a + b)
    print(result)
third_expression()

# Third task

print('Values of polynomials: ')
def a_polynomial():
    x = 3
    y = x ** 4 - 2 * (x ** 3) + x ** 2 + 1
    print(y)
a_polynomial()

def b_polynomial():
    x = 2
    y = x ** 6 + 3 * (x ** 4) - 5 * (x ** 2) + x + 1
    print(y)
b_polynomial()

def c_polynomial():
    x = -1
    y = 4 * (x ** 5) + 2 * (x ** 4) + 6 * (x ** 3) + 7 * (x ** 2) + x + 3
    print(y)
c_polynomial()

def d_polynomial():
    x = 2
    y = x ** 8 + 5 * (x ** 4) - 2 * (x ** 2) + x
    print(y)
d_polynomial()

def e_polynomial():
    x = 2
    y = x ** 9 + 2 * (x ** 6) + 3 * (x ** 3) - 5
    print(y)
e_polynomial()

# Fourth task

print('Finding of expressions values: ')
def a_to_find_z():
    x = -1
    y = 2
    z = (x ** 6) * (y ** 3) + (x ** 4) * (y ** 2) + x ** 2
    print(z)
a_to_find_z()

def b_to_find_z():
    x = 2
    y = -1
    z = ((x ** 2) * (y ** 2) + x ** 3 + y ** 3 + 3 * (x ** 2) * y
    + 3 * x * (y ** 2) + x ** 2 + 2 * x * y + y ** 2)
    print(z)
b_to_find_z()

# Fifth task

print('Solving of fifth task: ')
def a_expression():
    x = 3
    y = x ** 2 + x + 1 / x + 1 / (x ** 2)
    print(y)
a_expression()

def b_expression():
    x = 2
    y = x ** 16 + x ** 4
    print(y)
b_expression()

# Sixth task

print('Exchange of numbers: ')
def exchange_of_numbers():
    x = 2
    y = 3
    x, y = y, x
    print(x, ",", y)
exchange_of_numbers()

# Seventh task

print('Sorting of array: ')
def arr_sorting():
    a = 1
    b = 2
    c = 3
    d = 4
    array = [a, b, c, d]
    array[0], array[1] = array[1], array[0]
    array[1], array[2] = array[2], array[1]
    array[2], array[3] = array[3], array[2]
    print(array)
arr_sorting()

# Eighth task

print('Expressions from eighth task are solving a problem of numbers exchange: ')
def second_variant_to_exchange_numbers():
    x = 2
    y = 3
    x = x + y
    y = x - y
    x = x - y
    print(x, y)
second_variant_to_exchange_numbers()