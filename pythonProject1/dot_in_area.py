# task 2.9. Variant 5
from math import sqrt

# initializing of constant R and entering of values to make operations with
R = 2
x = float(input("Please, enter your x: "))
y = float(input("Please, enter your y: "))

# functions for finding borders of rhombus
def rhombus_bottom_y(x):
    return abs(x) - 2
def rhombus_top_y(x):
    return - abs(x) + 2

# border of circle
sqrt_from_circle = sqrt((x**2) + (y**2))

# checking if point is included in circle
if sqrt_from_circle <= R:
# checking if point is not into rhombus via separating top and bottom of rhombus.
    if y >= 0:
        if y >= rhombus_top_y(x):
            print("Yor dot is into pointed area")
        else:
            print("Your dot is not into pointed area")
    elif y < 0:
        if y <= rhombus_bottom_y(x):
            print("Yor dot is into pointed area")
        else:
            pass
    else:
        print("Your point is not into pointed area")
else:
    print("Your point is not into pointed area")
