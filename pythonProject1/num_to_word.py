# Task 2.26
# entering of the main number
number = input("Enter number of three numbers: ")

# checking of numbers length
if len(number) == 3:

# getting the last number and deleting of it from the main one
    decremented_number = int(int(number) % 10)
    number = number[:-1]

# writing down numbers as words and initialization of right hrivnias declension
    if decremented_number == 0:
        last_symbol = 0
        hrn = "гривень"
    elif decremented_number == 1:
        last_symbol = "одна"
        hrn = "гривня"
    elif decremented_number == 2:
        last_symbol = "дві"
        hrn = "гривні"
    elif decremented_number == 3:
        last_symbol = "три"
        hrn = "гривні"
    elif decremented_number == 4:
        last_symbol = "чотири"
        hrn = "гривні"
    elif decremented_number == 5:
        last_symbol = "п'ять"
        hrn = "гривень"
    elif decremented_number == 6:
        last_symbol = "шість"
        hrn = "гривень"
    elif decremented_number == 7:
        last_symbol = "сім"
        hrn = "гривень"
    elif decremented_number == 8:
        last_symbol = "вісім"
        hrn = "гривень"
    elif decremented_number == 9:
        last_symbol = "дев'ять"
        hrn = "гривень"

# repeat of the first operation for second symbol
    decremented_number = int(int(number) % 10)
    number = number[:-1]

# checking of the last symbol and assigning of second value that depends on last symbol
    if last_symbol == 0:
        if decremented_number == 0:
            second_symbol = ""
            last_symbol = ""
        if decremented_number == 1:
            second_symbol = "десять"
            last_symbol = ""
        if decremented_number == 2:
            second_symbol = "двадцять"
            last_symbol = ""
        if decremented_number == 3:
            second_symbol = "тридцять"
            last_symbol = ""
        if decremented_number == 4:
            second_symbol = "сорок"
            last_symbol = ""
        if decremented_number == 5:
            second_symbol = "п'ятдесят"
            last_symbol = ""
        if decremented_number == 6:
            second_symbol = "шістдесят"
            last_symbol = ""
        if decremented_number == 7:
            second_symbol = "сімдесят"
            last_symbol = ""
        if decremented_number == 8:
            second_symbol = "вісімдесят"
            last_symbol = ""
        if decremented_number == 9:
            second_symbol = "дев'яносто"
            last_symbol = ""
    else:
        if decremented_number == 0:
            second_symbol = ""
        if decremented_number == 1:
            second_symbol = "десять"
        if decremented_number == 2:
            second_symbol = "двадцять"
        if decremented_number == 3:
            second_symbol = "тридцять"
        if decremented_number == 4:
            second_symbol = "сорок"
        if decremented_number == 5:
            second_symbol = "п'ятдесят"
        if decremented_number == 6:
            second_symbol = "шістдесят"
        if decremented_number == 7:
            second_symbol = "сімдесят"
        if decremented_number == 8:
            second_symbol = "вісімдесят"
        if decremented_number == 9:
            second_symbol = "дев'яносто"

# operation of choosing previous number and its deleting
    decremented_number = int(int(number) % 10)
    number = number[:-1]

# changing of the first symbol to word
    if decremented_number == 0:
        first_symbol = ""
    elif decremented_number == 1:
        first_symbol = "сто"
    elif decremented_number == 2:
        first_symbol = "двісті"
    elif decremented_number == 3:
        first_symbol = "триста"
    elif decremented_number == 4:
        first_symbol = "чотириста"
    elif decremented_number == 5:
        first_symbol = "п'ятсот"
    elif decremented_number == 6:
        first_symbol = "шістсот"
    elif decremented_number == 7:
        first_symbol = "сімсот"
    elif decremented_number == 8:
        first_symbol = "вісімсот"
    elif decremented_number == 9:
        first_symbol = "дев'ятсот"

# output of value
    print(first_symbol, second_symbol, last_symbol, hrn)

# actions if length of entered number is bigger than three
else:
    print("You entered wrong number")