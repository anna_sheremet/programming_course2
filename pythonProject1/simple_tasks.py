# task 2.5

# initialization of values
a = 5
b = 3
c = 2
values = [a, b, c]
# sorting by increasing
values.sort()
print(values)

# sorting by decreasing
values.sort(reverse = True)
print(values)

# checking if values are sorted
values = [a, c, b]
if a < b < c or a > b > c:
    print("Your values are sorted by increasing or decreasing of numbers")
else:
    print("Your values are not sorted")

# task 2.6
# task a
n = 1
if n > 0 and type(n) is int and n % 2 == 1:
    print("This value is odd and natural")
else:
    print("This value is not odd and natural")

# task b
n = 13
m = 3
if n % 10 == m:
    print("m is the last number of n")
else:
    print("m is not last number of n")

# task c
n = 6
m = 2
if type(n) is int and n % m == 0:
    print("m devides n without remainder")
else:
    print("m devides n with remainder")

# task d
n = 8
k = 10
m = 2
if type(n) is int and type(k) is int and type(m) is int and n > 0 and k > 0 and n % m == 0 and k % m == 0:
    print("m devides n and k without remainder")
else:
    print("m devides n and k with remainder")

# task e
n = 238
a = 10
b = 20
sum = 0
while n > 0:
    last_number = n % 10
    sum = sum + last_number
    n = n // 10
if a < sum < b:
    print("Sum of numbers is between a and b")
else:
    print("Sum of numbers is out of section a-b")

# task f
x = 20
y = 2
if x > y + 6:
    print("x is bigger than y for more than for 6")
else:
    print("x is not bigger than y for more than 6")

# task g
x = 400
y = 50
z = 100
if x > 100 or y > 100 or z > 100:
    print("At least of one of your number is bigger than 100")
else:
    print("All of your numbers are less than 100")

# task h
if (x < 1000 and y >= 1000 and z >= 1000) or (x >= 1000 and y < 1000 and z >= 1000) or (x >= 1000 and y >= 1000 and z < 1000):
    print("Only one of your numbers is less than 1000")
else:
    print("Not only one of your numbers is less than 1000")


