# Lab08. Sheremet A.
# Variant-19

# Task variant - 4
"""4. Створити базовий клас "Товар". На його основі
реалізувати класи "Телевізор", "Телефон" та "Ноутбук". Класи
повинні мати можливість задавати та отримувати параметри
(вартість, модель, рік випуску тощо) задати за допомогою полів.
Для ноутбука повинна бути визначена діагональ екрана та тип
процесора, для Телевізора – діагональ екрана та тип матриці та
телефона – операційна система та кількість камер."""


class Device:
    def __init__(self):
        self._price = None
        self._model = None
        self._release_year = None

    @property
    def price(self):
        return self._price

    @property
    def model(self):
        return self._model

    @property
    def release_year(self):
        return self._release_year

    @price.setter
    def price(self, value):
        self._price = value

    @model.setter
    def model(self, value):
        self._model = value

    @release_year.setter
    def release_year(self, value):
        self._release_year = value


class Laptop(Device):
    def __init__(self, price, model, release_year, diagonal, processor_type):
        super(Laptop, self).__init__()
        self._diagonal = diagonal
        self._processor_type = processor_type
        self._price = price
        self._model = model
        self._release_year = release_year


    @property
    def diagonal(self):
        return self._diagonal

    @property
    def processor_type(self):
        return self._processor_type

    @diagonal.setter
    def diagonal(self, value):
        self._diagonal = value

    @processor_type.setter
    def processor_type(self, value):
        self._processor_type = value


class Televisor(Device):
    def __init__(self, price, model, release_year, diagonal, matrix_type):
        super(Televisor, self).__init__()
        self._diagonal = diagonal
        self._matrix_type = matrix_type
        self._price = price
        self._model = model
        self._release_year = release_year

    @property
    def diagonal(self):
        return self._diagonal

    @property
    def matrix_type(self):
        return self._matrix_type

    @diagonal.setter
    def diagonal(self, value):
        self._diagonal = value

    @matrix_type.setter
    def matrix_type(self, value):
        self._matrix_type = value


class MobilePhone(Device):
    def __init__(self, price, model, release_year, diagonal, operation_system, cameras_amount):
        super(MobilePhone, self).__init__()
        self._diagonal = diagonal
        self._operation_system = operation_system
        self._cameras_amount = cameras_amount
        self._price = price
        self._model = model
        self._release_year = release_year

    @property
    def diagonal(self):
        return self._diagonal

    @property
    def operation_system(self):
        return self._operation_system

    @property
    def cameras_amount(self):
        return self._cameras_amount

    @diagonal.setter
    def diagonal(self, value):
        self._diagonal = value

    @operation_system.setter
    def operation_system(self, value):
        self._operation_system = value

    @cameras_amount.setter
    def cameras_amount(self, value):
        self._cameras_amount = value


if __name__ == "__main__":
    laptop = Laptop(0, 0, 0, 0, 0)
    tv = Televisor(0, 0, 0, 0, 0)
    mobile = MobilePhone(0, 0, 0, 0, 0, 0)
    laptop.price = 1500
    laptop.model = "5A"
    laptop.release_year = 2022
    laptop.diagonal = 25
    laptop.processor_type = "Intel Core 7"
    tv.price = 1000
    tv.model = "LG"
    tv.release_year = 2020
    tv.diagonal = 120
    tv.matrix_type = "TN"
    mobile.price = 1200
    mobile.model = "Samsung"
    mobile.release_year = 2022
    mobile.diagonal = 12
    mobile.operation_system = "Microsoft"
    mobile.cameras_amount = 3
    print(laptop.price, laptop.model, laptop.release_year, laptop.diagonal, laptop.processor_type)
    print(tv.price, tv.model, tv.release_year, tv.diagonal, tv.matrix_type)
    print(mobile.price, mobile.model, mobile.release_year, mobile.diagonal, mobile.operation_system, mobile.cameras_amount)