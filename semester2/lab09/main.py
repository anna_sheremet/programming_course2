# Lab09. Sheremet A.
"""Завдання 1. Створіть файли, у яких будуть міститися
рядки з іменами студентів та їх середніми балами.
Реалізуйте читання файлів, запис та дозапис у файли,
пошук файлів у каталозі та пошук даних у файлі. Також
реалізуйте сортування даних у файлі за середнім балом."""
import os.path
import glob
from collections import OrderedDict
from pathlib import Path


class Journal:
    def __init__(self, catalog_path, group_file):
        self.group_file = group_file
        self.catalog_path = catalog_path
        self.path_to_file = os.path.join(self.catalog_path, self.group_file)

    def read_journal(self):
        file = open(self.path_to_file, 'r')
        print('\nFile ' + self.group_file + ':')
        for line in file:
            print(line, end='')
        file.close()

    def write_on_journal(self, last_name, mark):
        file = open(self.path_to_file, 'a')
        if type(last_name) == str and (type(mark) == float or type(mark) == int):
            line = f"{last_name} {float(mark)}"
            file.write(f"{line}\n")
            file.close()
        else:
            file.close()
            print("Ooops, name or mark has no appropriate type")

    def sort_journal(self, key_param):
        """This method rewrite your journal in alphabet or in ascending order.
        A key_param have to be 0 or 1"""
        file = open(self.path_to_file, 'r+')
        # alphabet order
        if key_param == 0:
            lines_list = file.readlines()
            sorting_dict = {line.split()[0]:line.strip() for line in lines_list}
            keysort = sorted([key for key in sorting_dict])
            file.seek(0)
            for key in keysort:
                print(sorting_dict[key])
                file.write(f"{sorting_dict[key]}\n")
            file.truncate()
        # ascending order
        elif key_param == 1:
            # An inner function below is dedicated to connect sorted array and surnames of students
            # keeping up with order.
            def match_arrays(sorted, with_surnames):
                arr = []
                for i in range(len(sorted)):
                    for j in range(len(with_surnames)):
                        if sorted[i] == with_surnames[j][1]:
                            arr.append(f"{with_surnames[j][0]} {sorted[i]}")
                        else:
                            j += 1
                return arr
            # preparations for sorting
            lines_list = file.readlines()
            sorting_arr = [line.split() for line in lines_list]
            keysort = sorted([key[1] for key in sorting_arr])
            result_arr = match_arrays(keysort, sorting_arr)
            # file's overwriting
            file.seek(0)
            results = list(OrderedDict.fromkeys(result_arr))
            for line in results:
                file.write(f"{line}\n")
            file.truncate()
        else:
            file.close()
            print("Ooops, not right key_param. Try 0 or 1")

    def find_data_in_journal(self, text):
        with open(self.path_to_file, 'r') as file:
            lines = file.readlines()
            for line in lines:
                if line.find(text) != -1:
                    print('Your data exists in file')
                    print('Line Number:', lines.index(line) + 1)


class Catalog:
    def __init__(self, catalog_path):
        self.catalog_path = catalog_path
        os.chdir(self.catalog_path)

    def find_journal_in_catalog(self, journal_name):
        if not glob.glob(f"{journal_name}*"):
            print(f"File \'{journal_name}\' does not exist in directory '{self.catalog_path}'")
        else:
            for file in glob.glob(f"{journal_name}*"):
                print('Absolute path:     ', os.path.abspath(file))


if __name__ == "__main__":
    jr = Journal("groups", "Group1.txt")
    jr.write_on_journal("Kolo", 4.6)
    jr.read_journal()
    jr.sort_journal(1)
    jr.read_journal()
    jr.find_data_in_journal("Sheremet")

    print("\n")

    ct = Catalog("groups")
    ct.find_journal_in_catalog("Group3")
    ct.find_journal_in_catalog("Group5")
