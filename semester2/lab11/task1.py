from tkinter import *
from tkinter import filedialog


def load_file():
    name = filedialog.askopenfilename()
    if str(name) != "()":
        f = open(name)
        s = f.read()
        t.insert(1.0, s)
        f.close()


def save_file():
    name = filedialog.asksaveasfilename(
        filetypes=(
            ("TXT files", "* .txt"),
        ))
    if str(name) != "":
        f = open(name, 'w')
        s = t.get(1.0, END)
        f.write(s)
        f.close()


root = Tk()
t = Text(width=30, height=5)
t.grid(columnspan=2)
b1 = Button(text="Відкрити", command=load_file)
b2 = Button(text="Зберегти", command=save_file)
b1.grid(row=1, sticky=E)
b2.grid(row=1, column=1, sticky=W)
root.mainloop()
