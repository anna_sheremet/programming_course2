from tkinter import *


def triangle():
    global current_shape
    canvas.delete(current_shape)
    current_shape = canvas.create_polygon(50, 200, 340, 200, 110, 60,
                                          fill='yellow', outline='white')
    text.delete('1.0', END)
    text.insert('1.0', 'Зображення трикутника')
    text.tag_add('title', '1.0', '1.end')
    text.tag_config('title', font=('Times', 14), foreground='blue')


def rectangle():
    global current_shape
    canvas.delete(current_shape)
    current_shape = canvas.create_rectangle(80, 50, 320, 200,
                                            fill='blue', outline='white')
    text.delete('1.0', END)
    text.insert('1.0', 'Зображення прямокутника')
    text.tag_add('title', '1.0', '1.end')
    text.tag_config('title', font=('Times', 14), foreground='black')


def circle():
    global current_shape
    canvas.delete(current_shape)
    current_shape = canvas.create_oval(50, 50, 300, 300,
                                       fill='red', outline='white')
    text.delete('1.0', END)
    text.insert('1.0', 'Зображення кола')
    text.tag_add('title', '1.0', '1.end')
    text.tag_config('title', font=('Times', 14), foreground='black')


def clear():
    global current_shape
    canvas.delete(ALL)
    text.delete('1.0', END)
    current_shape = None



root = Tk()

b_triangle = Button(text="Трикутник", width=15, command=triangle)
b_rectangle = Button(text="Прямокутник", width=15, command=rectangle)
b_circle = Button(text="Коло", width=15, command=circle)
b_clear = Button(text="Очистити", width=15, command=clear)

canvas = Canvas(width=400, height=300, bg='#fff')
text = Text(width=55, height=5, bg='#fff', wrap=WORD)
current_shape = None


b_triangle.grid(row=1, column=0)
b_rectangle.grid(row=2, column=0)
b_circle.grid(row=3, column=0)
b_clear.grid(row=4, column=0)

canvas.grid(row=0, column=1, rowspan=10)
text.grid(row=11, column=1, rowspan=3)
root.mainloop()
