from numpy import *
import matplotlib.pyplot as plt
from numpy import linspace

t = linspace(0, 3, 51)
y = t ** 2 * exp(-t ** 2)
plt.plot(t, y, 'r--', marker="o", ms="10", markeredgecolor="y", lw="5",  label='t^2*exp(-t^2)')
plt.axis([0, 3, -0.05, 0.5])
plt.xlabel('t')
plt.ylabel('y')
plt.title('My first normal plot')
plt.legend()
plt.show()
