from numpy import *
import matplotlib.pyplot as plt

x = array(linspace(-2, 2, 100))
y = sin(x) * (1 / x) * cos((x ** 2) + (1 / x))
plt.grid(color="gray")
plt.plot(x, y, "g", label="y(x)=sin(x) * (1 / x) * cos((x ** 2) + (1 / x))")
plt.title("Graph")
plt.xlabel("x")
plt.ylabel("y")
plt.legend(loc='upper right', borderpad=2,)
plt.show()
